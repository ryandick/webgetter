var axon = require( 'axon' );
var Sockets = function () {
	var $this = this;
	$this._sock = axon.socket( 'req' );
	//  Methods
	$this.init = function ( port ) {
		$this.port = port || 3100;
		//	setup messaging socket
		$this._sock.bind( $this.port );
	};
	$this.dispatch = function(msg, callback){
		$this._sock.send(msg, callback);
	};
	return $this;
};
module.exports = Sockets;