var Downloader = require( '../lib/download' );
var checksum = require( '../lib/checksum' );
var path = require( 'path' );
var url = require( 'url' );
//  url to download from arguments
var urlToDl = 'http://rosebank.spdtst.saix.net/speedtest/random2000x2000.jpg';
var md5Sum = 'a1684971de0de7327037f09fe0e1da3eeeae4115';
//  cocurrency
var cocurrentDls = 10;
//  file name to save to
var fileName = url.parse( urlToDl ).pathname;
//  remove the rest of the path from file name
fileName = fileName.substring( fileName.lastIndexOf( '/' ) + 1, fileName.length );
//  get path to save file to
var downloadFolder = path.resolve( 'downloads/' + fileName );
var startTime = new Date().getTime();
console.log( 'starting download of: "' + urlToDl + '" to file: "' + fileName + '" in directory: "' + downloadFolder + '" using: ' + cocurrentDls + ' workers...' );
//  begin downloading
Downloader( urlToDl, downloadFolder, cocurrentDls, function ( ret ) {
	var fileChecksum = checksum.file( downloadFolder, function ( err, sum ) {
		if ( err ) throw err;
		else {
			if ( sum == md5Sum ) {
				var endTime = new Date().getTime();
				var diff = endTime - startTime;
                                                    var rate = Math.round(((1000 * 100)/diff)*100)/100;
				console.log( 'file checksum passed, download complete! Time: ' + diff +'ms, transfer rate: '+ rate+'kb/s');
			} else {
				throw 'checksum did not match file is damaged';
			}
		}
	} )
} );