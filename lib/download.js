//  Wraps multipart downloader module
var download = function ( url, dest, count, cb ) {
	var multipartDL = require( "./multiDL" );
	multipartDL.download( dest, url, count,null, cb );
}
module.exports = download;