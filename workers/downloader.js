///////////////////////////////////////////
//  Quick and dirty multipart downloader //
///////////////////////////////////////////
var axon = require( 'axon' );
var sock = axon.socket( 'rep' );
sock.connect( 3100 );
//  event handlers
sock.on( 'close', function () {
	console.log( 'download-worker socket close' );
} );
sock.on( 'error', function ( err ) {
	console.log( 'download-worker socket error: ' + err );
} );
sock.on( 'ignored error', function ( err ) {
	console.log( 'download-worker socket ignore error: ' + err );
} );
sock.on( 'socket error', function ( err ) {
	console.log( 'download-worker socket error: ' + err );
} );
sock.on( 'reconnect attempt', function () {
	console.log( 'download-worker attempting to reconnect' );
} );
sock.on( 'connect', function () {
	console.log( 'download-worker connected' );
} );
sock.on( 'disconnect', function () {
	console.log( 'download-worker disconnected' );
} );
sock.on( 'bind', function () {
	console.log( 'download-worker port bound' );
} );
sock.on( 'drop', function ( msg ) {
	console.log( 'download-worker dropping message due to HWM, message: ' + msg );
} );
sock.on( 'flush', function ( msg ) {
	console.log( 'download-worker queue flushed on connect, ' + msg );
} );
//  on work
sock.on( 'message', function ( urlToDl, res ) {
	urlToDl = urlToDl.toString( 'utf8' );
	var Downloader = require( '../lib/download' );
	var path = require( 'path' );
	var url = require( 'url' );
	//  cocurrency
	var cocurrentDls = 25;
	//  file name to save to
	var fileName = url.parse( urlToDl ).pathname;
	//  remove the rest of the path from file name
	fileName = fileName.substring( fileName.lastIndexOf( '/' ) + 1, fileName.length );
	//  get path to save file to
	var downloadFolder = path.resolve( 'downloads/' + fileName );
	console.log( 'starting download of: "' + urlToDl + '" to file: "' + fileName + '" in directory: "' + downloadFolder + '" using: ' + cocurrentDls + ' workers...' );
	//  begin downloading
	Downloader( urlToDl, downloadFolder, cocurrentDls, function ( d ) {
		res( downloadFolder.toString('utf8') );
	} );
} );