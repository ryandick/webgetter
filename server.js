var express = require( 'express' ),
	http = require( 'http' ),
	api = require( './routes/api' ),
	path = require( 'path' );


var app = module.exports = express();
/**
 * Configuration
 */
// all environments
app.set( 'port', process.env.PORT || 3000 );
app.use( express.logger( 'dev' ) );
app.use( express.bodyParser() );
app.use( express.methodOverride() );
app.use( app.router );
/**
 * Routes
 */
app.post( '/download', api.startDownloading )
/**
 * Start Server
 */
http.createServer( app ).listen( app.get( 'port' ), function () {
	console.log( 'Express server listening on port ' + app.get( 'port' ) );
} );